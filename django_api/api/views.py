from django.shortcuts import render

# Create your views here.
from rest_framework import viewsets, generics
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from django.http import Http404
from musicportal.models import Playlist, Tracks, Employees, Invoices, Genres, MediaTypes, Albums, Artists, Customers,\
    InvoiceItems, PriceHistory, PlaylistTrack
from api.serializers import ArtistsSerializer, GenresSerializer, AlbumsSerializer, TracksSerializer, \
    PlaylistTrackSerializer, PlaylistSerializer,InvoiceItemsSerializer, MediaTypesSerializer, InvoicesSerializer, \
    CustomersSerializer, EmployeesSerializer, PriceHistorySerializer
from rest_framework.decorators import api_view
from django.utils.timezone import timedelta
from django.utils import timezone
import datetime


class ArtistsViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = Artists.objects.all()
    serializer_class = ArtistsSerializer


# class ArtistsList(APIView):
#     """
#     List all pricehistory, or create a new pricehistory.
#     """
#
#     def get(self, request, format=None):
#         artists = Artists.objects.all()
#         serializer = ArtistsSerializer(artists, many=True)
#         return Response(serializer.data)
#
#
# class ArtistsDetail(APIView):
#     """
#     Retrieve, update or delete a snippet instance.
#     """
#     def get_object(self, pk):
#         try:
#             return Artists.objects.get(pk=pk)
#         except Artists.DoesNotExist:
#             raise Http404
#
#     def get(self, request, pk, format=None):
#         artist = self.get_object(pk)
#         serializer = ArtistsSerializer(artist)
#         return Response(serializer.data)


class GenresViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = Genres.objects.all()
    serializer_class = GenresSerializer


class AlbumsViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = Albums.objects.all()
    serializer_class = AlbumsSerializer


class MediaTypesViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = MediaTypes.objects.all()
    serializer_class = MediaTypesSerializer


class TracksViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = Tracks.objects.all()
    serializer_class = TracksSerializer


class PlaylistViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = Playlist.objects.all()
    serializer_class = PlaylistSerializer


class PlaylistTrackViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = PlaylistTrack.objects.all()
    serializer_class = PlaylistTrackSerializer


class InvoiceItemsViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = InvoiceItems.objects.all()
    serializer_class = InvoiceItemsSerializer


class InvoicesViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = Invoices.objects.all()
    serializer_class = InvoicesSerializer


class CustomersViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = Customers.objects.all()
    serializer_class = CustomersSerializer


class EmployeesViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = Employees.objects.all()
    serializer_class = EmployeesSerializer


# class PriceHistoryViewSet(viewsets.ModelViewSet):
#     """
#     API endpoint that allows groups to be viewed or edited.
#     """
#     queryset = PriceHistory.objects.all()
#     serializer_class = PriceHistorySerializer
from django.core.exceptions import ObjectDoesNotExist


class PriceHistoryList(APIView):
    """
    List all pricehistory, or create a new pricehistory.
    """

    def start_and_end_date_of_week(self):
        date_today = datetime.datetime.today()
        day_week_today = datetime.datetime.weekday(date_today)  # (monday-0...sunday-6)
        start_date = date_today - timedelta(days=day_week_today)
        end_date = start_date + timedelta(days=6)
        range_date = [(start_date + datetime.timedelta(days=x)).strftime('%Y-%m-%d') for x in range(0, (end_date - start_date).days + 1)]
        list_date = [start_date, end_date, range_date]
        return list_date

    def get(self, request, format=None):
        list_date = self.start_and_end_date_of_week()
        qweryset = PriceHistory.objects.filter(timestamp__range=list_date[:2]).order_by('timestamp')
        exist_dates = [(exist_date.timestamp).strftime('%Y-%m-%d') for exist_date in qweryset]
        empty_dates = list(set(list_date[2]).difference(set(exist_dates)))
        if empty_dates:
            message = [{}.fromkeys(empty_dates, "На дану дату запису не має!")]
        else:
            message = []
        serializer = PriceHistorySerializer(qweryset, many=True)
        return Response(serializer.data+message)

    def post(self, request, format=None):
        timestamp = request.data.get('timestamp', None)
        if not timestamp:
            timestamp = datetime.datetime.today().strftime('%Y-%m-%d')
        try:
            price_history = PriceHistory.objects.filter(timestamp=timestamp).first()
            serializer = PriceHistorySerializer(price_history, data=request.data)
        except PriceHistory.DoesNotExist:
            serializer = PriceHistorySerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class PriceHistoryDetail(APIView):
    """
    Retrieve pricehistory instance.
    """
    def get_object(self, pk):
        try:
            return PriceHistory.objects.get(pk=pk)
        except PriceHistory.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        price_history = self.get_object(pk)
        serializer = PriceHistorySerializer(price_history)
        return Response(serializer.data)



