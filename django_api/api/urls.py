from django.urls import include, re_path,path
from rest_framework import routers
from . import views

router = routers.DefaultRouter()
router.register(r'artists', views.ArtistsViewSet)
router.register(r'genres', views.GenresViewSet)
router.register(r'albums', views.AlbumsViewSet)
router.register(r'media-types', views.MediaTypesViewSet)
router.register(r'tracks', views.TracksViewSet)
router.register(r'playlist', views.PlaylistViewSet)
router.register(r'playlist-track', views.PlaylistTrackViewSet)
router.register(r'invoice-items', views.InvoiceItemsViewSet)
router.register(r'invoices', views.InvoicesViewSet)
router.register(r'customers', views.CustomersViewSet)
router.register(r'employees', views.EmployeesViewSet)
# router.register(r'price-history', views.PriceHistoryViewSet)

urlpatterns = [
    re_path(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    re_path(r'^pricehistory/$', views.PriceHistoryList.as_view()),
    re_path(r'^pricehistory/(?P<pk>\d+)/$', views.PriceHistoryDetail.as_view()),
    # re_path(r'^artists/$', views.ArtistsList.as_view()),
    # re_path(r'^artists/(?P<pk>\d+)/$', views.ArtistsDetail.as_view()),
    re_path(r'^', include(router.urls))
]
