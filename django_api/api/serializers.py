
from musicportal.models import Playlist, Tracks, Employees, Invoices, Genres, MediaTypes, Albums, Artists, Customers, InvoiceItems, PriceHistory, PlaylistTrack
from rest_framework import serializers
from django.utils.timezone import datetime, timedelta
import datetime
from django.utils import timezone
from rest_framework.validators import ValidationError


class ArtistsSerializer(serializers.HyperlinkedModelSerializer):
    """
    Serializing(PlaylistSerializer) all the Playlist
    """

    class Meta:
        model = Artists
        # fields = ('playlist_id', 'name', 'members')
        fields = '__all__'


# class ArtistsSerializer(serializers.ModelSerializer):
#     """
#     Serializing(PlaylistSerializer) all the Playlist
#     """
#     # url = serializers.HyperlinkedIdentityField(view_name="api:artists")
#     class Meta:
#         model = Artists
#         # fields = ('playlist_id', 'name', 'members')
#         fields = '__all__'


class GenresSerializer(serializers.HyperlinkedModelSerializer):
    """
    Serializing(GenresSerializer) all the Genres
    """

    class Meta:
        model = Genres
        fields = '__all__'


class AlbumsSerializer(serializers.HyperlinkedModelSerializer):
    """
    Serializing(AlbumsSerializer) all the Albums
    """

    class Meta:
        model = Albums
        fields = '__all__'


class MediaTypesSerializer(serializers.HyperlinkedModelSerializer):
    """
    Serializing(TracksSerializer) all the Tracks
    """

    class Meta:
        model = MediaTypes
        fields = '__all__'


class TracksSerializer(serializers.HyperlinkedModelSerializer):
    """
    Serializing(TracksSerializer) all the Tracks
    """

    class Meta:
        model = Tracks
        fields = '__all__'


class PlaylistSerializer(serializers.HyperlinkedModelSerializer):
    """
    Serializing(PlaylistSerializer) all the Tracks
    """

    class Meta:
        model = Playlist
        fields = '__all__'


class PlaylistTrackSerializer(serializers.HyperlinkedModelSerializer):
    """
    Serializing(PlaylistTrackSerializer) all the PlaylistTrack
    """

    class Meta:
        model = PlaylistTrack
        fields = '__all__'


class InvoiceItemsSerializer(serializers.HyperlinkedModelSerializer):
    """
    Serializing(TracksSerializer) all the Tracks
    """

    class Meta:
        model = InvoiceItems
        fields = '__all__'


class InvoicesSerializer(serializers.HyperlinkedModelSerializer):
    """
    Serializing(TracksSerializer) all the Tracks
    """

    class Meta:
        model = Invoices
        fields = '__all__'


class CustomersSerializer(serializers.HyperlinkedModelSerializer):
    """
    Serializing(TracksSerializer) all the Tracks
    """

    class Meta:
        model = Customers
        fields = '__all__'


class EmployeesSerializer(serializers.HyperlinkedModelSerializer):
    """
    Serializing(TracksSerializer) all the Tracks
    """

    class Meta:
        model = Employees
        fields = '__all__'


def validate_entered_date(value):
    """
    Check if date does not exceed on week in future
    """
    if value >= (datetime.date.today() + timedelta(days=7)):
        raise ValidationError("Ви перевищили максимальну дату. Спробуйте встановити дату до: {} періоду".format(datetime.date.today() + timedelta(days=7)))


class PriceHistorySerializer(serializers.ModelSerializer):
    """
    Serializing(TracksSerializer) all the Tracks
    """
    timestamp = serializers.DateField(format="%Y-%m-%d", input_formats=["%Y-%m-%d"], default=serializers.CreateOnlyDefault(datetime.date.today), validators=[validate_entered_date])

    class Meta:
        model = PriceHistory
        fields = '__all__'
