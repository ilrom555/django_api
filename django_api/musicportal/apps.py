from django.apps import AppConfig


class MusicportalConfig(AppConfig):
    name = 'musicportal'
