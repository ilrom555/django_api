from django.contrib import admin
from .models import Playlist, Tracks, Employees, Invoices, Genres, MediaTypes, Albums, Artists, Customers, InvoiceItems, PriceHistory, PlaylistTrack
# Register your models here.

admin.site.register(Playlist)
admin.site.register(Tracks)
admin.site.register(Employees)
admin.site.register(Invoices)
admin.site.register(Genres)
admin.site.register(MediaTypes)
admin.site.register(Albums)
admin.site.register(Artists)
admin.site.register(Customers)
admin.site.register(InvoiceItems)
admin.site.register(PriceHistory)
admin.site.register(PlaylistTrack)




