from django.db import models

# Create your models here.


class NvarcharField(models.CharField):
    def db_type(self, connection):
        return 'nvarchar({})'.format(self.max_length)


class NumericField(models.DecimalField):
    def db_type(self, connection):
        return 'numeric({},{})'.format(self.max_digits, self.decimal_places)


class Artists(models.Model):
    artist_id = models.AutoField(db_column='ArtistId', primary_key=True, null=False)
    name = NvarcharField(db_column='Name', max_length=160, blank=True, null=True)

    class Meta:
        db_table = 'artists'
        managed = False


class Albums(models.Model):
    album_id = models.AutoField(db_column='AlbumId', primary_key=True, null=False)
    title = NvarcharField(db_column='Title', max_length=160, blank=True, null=False)
    artist_id = models.ForeignKey(Artists, on_delete=models.DO_NOTHING, db_column='ArtistId')

    class Meta:
        db_table = 'albums'
        managed = False


class Customers(models.Model):
    customer_id = models.AutoField(db_column='CustomerId', primary_key=True)
    firstname =NvarcharField(db_column='FirstName', max_length=40, null=False)
    lastname = NvarcharField(db_column='LastName', max_length=20, null=False)
    company = NvarcharField(db_column='Company', max_length=80, blank=True, null=True)
    address = NvarcharField(db_column='Address', max_length=70, blank=True, null=True)
    city = NvarcharField(db_column='City', max_length=40, blank=True, null=True)
    state = NvarcharField(db_column='State', max_length=40, blank=True, null=True)
    country = NvarcharField(db_column='Country', max_length=40, blank=True, null=True)
    postalcode = NvarcharField(db_column='PostalCode', max_length=10, blank=True, null=True)
    phone = NvarcharField(db_column='Phone', max_length=24, blank=True, null=True)
    fax = NvarcharField(db_column='Fax', max_length=24, blank=True, null=True)
    email = NvarcharField(db_column='Email', max_length=60, null=False)
    supportrep_id = models.ForeignKey('Employees', on_delete=models.DO_NOTHING, db_column='SupportRepId', blank=True, null=True)

    class Meta:
        db_table = 'customers'
        managed = False


class Employees(models.Model):
    employee_id = models.AutoField(db_column='EmployeeId', primary_key=True)
    lastname = NvarcharField(db_column='LastName', max_length=20, null=False)
    firstname = NvarcharField(db_column='FirstName', max_length=20, null=False)
    title = NvarcharField(db_column='Title', max_length=30, blank=True, null=True)
    reportsto = models.ForeignKey('self', on_delete=models.DO_NOTHING, db_column='ReportsTo', blank=True, null=True)
    birthdate = models.DateTimeField(db_column='BirthDate', blank=True, null=True)
    hiredate = models.DateTimeField(db_column='HireDate', blank=True, null=True)
    address = NvarcharField(db_column='Address', max_length=70, blank=True, null=True)
    city = NvarcharField(db_column='City', max_length=40, blank=True, null=True)
    state = NvarcharField(db_column='State', max_length=40, blank=True, null=True)
    country = NvarcharField(db_column='Country', max_length=40, blank=True, null=True)
    postalcode = NvarcharField(db_column='PostalCode', max_length=10, blank=True, null=True)
    phone = NvarcharField(db_column='Phone', max_length=24, blank=True, null=True)
    fax = NvarcharField(db_column='Fax', max_length=24, blank=True, null=True)
    email = NvarcharField(db_column='Email', max_length=60, blank=True, null=True)

    class Meta:
        db_table = 'employees'
        managed = False


class Genres(models.Model):
    genre_id = models.IntegerField(db_column='GenreId', primary_key=True)
    name = NvarcharField(db_column='Name', max_length=120)

    class Meta:
        db_table = 'genres'
        managed = False


class InvoiceItems(models.Model):
    invoiceline_id = models.AutoField(db_column='InvoiceLineId', primary_key=True)
    invoiceid = models.IntegerField(db_column='InvoiceId', null=False)
    trackid = models.ForeignKey('Invoices', on_delete=models.DO_NOTHING, db_column='TrackId')
    unitprice = NumericField(db_column='UnitPrice', max_digits=12, decimal_places=2, null=False)
    quantity = models.IntegerField(db_column='Quantity', null=False)

    class Meta:
        db_table = 'invoice_items'
        managed = False


class Invoices(models.Model):
    invoice_id = models.AutoField(db_column='InvoiceId', primary_key=True)
    customer_id = models.ForeignKey(Customers, on_delete=models.DO_NOTHING, db_column='CustomerId')
    invoicedate = models.DateTimeField(db_column='InvoiceDate', null=False)
    billingaddress = NvarcharField(db_column='BillingAddress', max_length=70, blank=True, null=True)
    billingcity = NvarcharField(db_column='BillingCity', max_length=40, blank=True, null=True)
    billingstate = NvarcharField(db_column='BillingState', max_length=40, blank=True, null=True)
    billingcountry = NvarcharField(db_column='BillingCountry', max_length=40, blank=True, null=True)
    billingpostalcode = NvarcharField(db_column='BillingPostalCode', max_length=10, blank=True, null=True)
    total = NumericField(db_column='Total', max_digits=12, decimal_places=2, null=False)

    class Meta:
        db_table = 'invoices'
        managed = False


class MediaTypes(models.Model):
    mediatype_id = models.AutoField(db_column='MediaTypeId', primary_key=True)
    name = NvarcharField(db_column='Name', max_length=120)

    class Meta:
        db_table = 'media_types'
        managed = False


class PlaylistTrack(models.Model):
    playlisttrack_id = models.OneToOneField('Playlist', on_delete=models.CASCADE, primary_key=True, db_column='PlaylistId')
    track_id = models.OneToOneField('Tracks', on_delete=models.CASCADE, db_column='TrackId')

    class Meta:
        db_table = 'playlist_track'
        managed = False
        unique_together = (('playlisttrack_id', 'track_id'),)


class Playlist(models.Model):
    playlist_id = models.AutoField(db_column='PlaylistId', primary_key=True)
    name = NvarcharField(db_column='Name', max_length=120, blank=True, null=True)
    members = models.ManyToManyField('Tracks', through='PlaylistTrack', through_fields=('playlisttrack_id', 'track_id'),
    )

    class Meta:
        db_table = 'playlists'
        managed = False


class PriceHistory(models.Model):
    pricehistory_id = models.AutoField(db_column='id', primary_key=True, unique=True)
    price = NumericField(db_column='price', blank=True, null=True, max_digits=12, decimal_places=2)
    track = models.ForeignKey('Tracks', on_delete=models.DO_NOTHING, db_column='track')
    timestamp = models.DateField(db_column='timestamp', blank=True)

    class Meta:
        db_table = 'price_history'
        managed = False


class Tracks(models.Model):
    track_id = models.AutoField(db_column='TrackId', primary_key=True)
    name = NvarcharField(db_column='Name', max_length=200, null=False)
    album_id = models.ForeignKey(Albums, on_delete=models.DO_NOTHING, blank=True, null=True, db_column='AlbumId')
    mediatype_id = models.ForeignKey(MediaTypes, on_delete=models.DO_NOTHING, db_column='MediaTypeId')
    genre_id = models.ForeignKey(Genres, on_delete=models.DO_NOTHING,blank=True, null=True, db_column='GenreId')
    composer = NvarcharField(db_column='Composer', blank=True, null=True, max_length=220)
    milliseconds = models.IntegerField(db_column='Milliseconds', null=False)
    bytes = models.IntegerField(db_column='Bytes', blank=True, null=True)
    unitprice = NumericField(db_column='UnitPrice', max_digits=12, decimal_places=2, null=False)

    class Meta:
        db_table = 'tracks'
        managed = False

