# Generated by Django 2.1.2 on 2018-10-16 19:18

from django.db import migrations, models
import django.db.models.deletion
import musicportal.models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Albums',
            fields=[
                ('album_id', models.AutoField(db_column='AlbumId', primary_key=True, serialize=False)),
                ('title', musicportal.models.NvarcharField(blank=True, db_column='Title', max_length=160)),
            ],
            options={
                'db_table': 'albums',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='Artists',
            fields=[
                ('artist_id', models.AutoField(db_column='ArtistId', primary_key=True, serialize=False)),
                ('name', musicportal.models.NvarcharField(blank=True, db_column='Name', max_length=160, null=True)),
            ],
            options={
                'db_table': 'artists',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='Customers',
            fields=[
                ('customer_id', models.AutoField(db_column='CustomerId', primary_key=True, serialize=False)),
                ('firstname', musicportal.models.NvarcharField(db_column='FirstName', max_length=40)),
                ('lastname', musicportal.models.NvarcharField(db_column='LastName', max_length=20)),
                ('company', musicportal.models.NvarcharField(blank=True, db_column='Company', max_length=80, null=True)),
                ('address', musicportal.models.NvarcharField(blank=True, db_column='Address', max_length=70, null=True)),
                ('city', musicportal.models.NvarcharField(blank=True, db_column='City', max_length=40, null=True)),
                ('state', musicportal.models.NvarcharField(blank=True, db_column='State', max_length=40, null=True)),
                ('country', musicportal.models.NvarcharField(blank=True, db_column='Country', max_length=40, null=True)),
                ('postalcode', musicportal.models.NvarcharField(blank=True, db_column='PostalCode', max_length=10, null=True)),
                ('phone', musicportal.models.NvarcharField(blank=True, db_column='Phone', max_length=24, null=True)),
                ('fax', musicportal.models.NvarcharField(blank=True, db_column='Fax', max_length=24, null=True)),
                ('email', musicportal.models.NvarcharField(db_column='Email', max_length=60)),
            ],
            options={
                'db_table': 'customers',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='Employees',
            fields=[
                ('employee_id', models.AutoField(db_column='EmployeeId', primary_key=True, serialize=False)),
                ('lastname', musicportal.models.NvarcharField(db_column='LastName', max_length=20)),
                ('firstname', musicportal.models.NvarcharField(db_column='FirstName', max_length=20)),
                ('title', musicportal.models.NvarcharField(blank=True, db_column='Title', max_length=30, null=True)),
                ('birthdate', models.DateTimeField(blank=True, db_column='BirthDate', null=True)),
                ('hiredate', models.DateTimeField(blank=True, db_column='HireDate', null=True)),
                ('address', musicportal.models.NvarcharField(blank=True, db_column='Address', max_length=70, null=True)),
                ('city', musicportal.models.NvarcharField(blank=True, db_column='City', max_length=40, null=True)),
                ('state', musicportal.models.NvarcharField(blank=True, db_column='State', max_length=40, null=True)),
                ('country', musicportal.models.NvarcharField(blank=True, db_column='Country', max_length=40, null=True)),
                ('postalcode', musicportal.models.NvarcharField(blank=True, db_column='PostalCode', max_length=10, null=True)),
                ('phone', musicportal.models.NvarcharField(blank=True, db_column='Phone', max_length=24, null=True)),
                ('fax', musicportal.models.NvarcharField(blank=True, db_column='Fax', max_length=24, null=True)),
                ('email', musicportal.models.NvarcharField(blank=True, db_column='Email', max_length=60, null=True)),
            ],
            options={
                'db_table': 'employees',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='Genres',
            fields=[
                ('genre_id', models.IntegerField(db_column='GenreId', primary_key=True, serialize=False)),
                ('name', musicportal.models.NvarcharField(db_column='Name', max_length=120)),
            ],
            options={
                'db_table': 'genres',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='InvoiceItems',
            fields=[
                ('invoiceline_id', models.AutoField(db_column='InvoiceLineId', primary_key=True, serialize=False)),
                ('invoiceid', models.IntegerField(db_column='InvoiceId')),
                ('unitprice', musicportal.models.NumericField(db_column='UnitPrice', decimal_places=2, max_digits=12)),
                ('quantity', models.IntegerField(db_column='Quantity')),
            ],
            options={
                'db_table': 'invoice_items',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='Invoices',
            fields=[
                ('invoice_id', models.AutoField(db_column='InvoiceId', primary_key=True, serialize=False)),
                ('invoicedate', models.DateTimeField(db_column='InvoiceDate')),
                ('billingaddress', musicportal.models.NvarcharField(blank=True, db_column='BillingAddress', max_length=70, null=True)),
                ('billingcity', musicportal.models.NvarcharField(blank=True, db_column='BillingCity', max_length=40, null=True)),
                ('billingstate', musicportal.models.NvarcharField(blank=True, db_column='BillingState', max_length=40, null=True)),
                ('billingcountry', musicportal.models.NvarcharField(blank=True, db_column='BillingCountry', max_length=40, null=True)),
                ('billingpostalcode', musicportal.models.NvarcharField(blank=True, db_column='BillingPostalCode', max_length=10, null=True)),
                ('total', musicportal.models.NumericField(db_column='Total', decimal_places=2, max_digits=12)),
            ],
            options={
                'db_table': 'invoices',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='MediaTypes',
            fields=[
                ('mediatype_id', models.AutoField(db_column='MediaTypeId', primary_key=True, serialize=False)),
                ('name', musicportal.models.NvarcharField(db_column='Name', max_length=120)),
            ],
            options={
                'db_table': 'media_types',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='Playlist',
            fields=[
                ('playlist_id', models.AutoField(db_column='PlaylistId', primary_key=True, serialize=False)),
                ('name', musicportal.models.NvarcharField(blank=True, db_column='Name', max_length=120, null=True)),
            ],
            options={
                'db_table': 'playlists',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='PriceHistory',
            fields=[
                ('pricehistory_id', models.AutoField(db_column='id', primary_key=True, serialize=False, unique=True)),
                ('price', musicportal.models.NumericField(blank=True, db_column='price', decimal_places=2, max_digits=12, null=True)),
                ('timestamp', models.DecimalField(db_column='timestamp', decimal_places=2, max_digits=12)),
            ],
            options={
                'db_table': 'price_history',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='Tracks',
            fields=[
                ('track_id', models.AutoField(db_column='TrackId', primary_key=True, serialize=False)),
                ('name', musicportal.models.NvarcharField(db_column='Name', max_length=200)),
                ('composer', musicportal.models.NvarcharField(blank=True, db_column='Composer', max_length=220, null=True)),
                ('milliseconds', models.IntegerField(db_column='Milliseconds')),
                ('bytes', models.IntegerField(blank=True, db_column='Bytes', null=True)),
                ('unitprice', musicportal.models.NumericField(db_column='UnitPrice', decimal_places=2, max_digits=12)),
            ],
            options={
                'db_table': 'tracks',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='PlaylistTrack',
            fields=[
                ('playlisttrack_id', models.OneToOneField(db_column='PlaylistId', on_delete=django.db.models.deletion.CASCADE, primary_key=True, serialize=False, to='musicportal.Playlist')),
            ],
            options={
                'db_table': 'playlist_track',
                'managed': False,
            },
        ),
    ]
